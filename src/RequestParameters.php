<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa;

/**
 * Trait RequestParameters
 * @package Omnipay\Omnikassa
 */
trait RequestParameters
{
    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->getParameter('language');
    }

    /**
     * @param string $language
     * @return mixed
     */
    public function setLanguage(string $language)
    {
        return $this->setParameter('language', $language);
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->getParameter('refreshToken');
    }

    /**
     * @param string $token
     * @return mixed
     */
    public function setRefreshToken(string $token)
    {
        return $this->setParameter('refreshToken', $token);
    }

    /**
     * @return string
     */
    public function getSigningKey(): string
    {
        return $this->getParameter('signingKey');
    }

    /**
     * @param string $signingKey
     * @return mixed
     */
    public function setSigningKey(string $signingKey)
    {
        return $this->setParameter('signingKey', $signingKey);
    }

    /**
     * @return string
     */
    public function getTimestamp(): string
    {
        return $this->getParameter('timestamp');
    }

    /**
     * @param string $timestamp
     * @return mixed
     */
    public function setTimestamp(string $timestamp)
    {
        return $this->setParameter('timestamp', $timestamp);
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodForced(): bool
    {
        return $this->getParameter('paymentMethodForced');
    }

    /**
     * @param bool $paymentMethodForced
     * @return mixed
     */
    public function setPaymentMethodForced(bool $paymentMethodForced)
    {
        return $this->setParameter('paymentMethodForced', $paymentMethodForced);
    }

}