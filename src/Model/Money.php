<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Model;

/**
 * Class Money
 * @package Omnipay\Omnikassa\Model
 */
class Money
{
    /**
     * @var string
     */
    private $currency;

    /**
     * @var int
     */
    private $amount;

    /**
     * Create a new money object with the amount in cents
     * @param string $currency      The money currency
     * @param int    $amount        The amount of money in cents
     */
    public function __construct(string $currency, int $amount)
    {
        $this->currency = $currency;
        $this->amount = $amount;
    }

    /**
     * Create a new money object with the amount as a decimal
     * @param string $currency      The money currency
     * @param float  $amount        The amount of money as a decimal
     * @return Money
     */
    public static function fromDecimal(string $currency, float $amount)
    {
        return new Money($currency, intval(round($amount, 2) * 100));
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}