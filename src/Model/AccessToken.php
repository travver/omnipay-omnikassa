<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Model;

use DateTime;

/**
 * Class AccessToken
 * @package Omnipay\Omnikassa\Model
 */
class AccessToken
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var DateTime
     */
    private $validUntil;

    /**
     * @var int
     */
    private $durationInMillis;

    /**
     * AccessToken constructor.
     * @param string $token
     * @param string $validUntil
     * @param int    $durationInMillis
     */
    public function __construct(string $token, string $validUntil, int $durationInMillis)
    {
        $this->token = $token;
        $this->validUntil = new DateTime($validUntil);
        $this->durationInMillis = $durationInMillis;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return DateTime
     */
    public function getValidUntil(): DateTime
    {
        return $this->validUntil;
    }

    /**
     * @return int
     */
    public function getDurationInMillis(): int
    {
        return $this->durationInMillis;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return (new DateTime())->diff($this->validUntil)->invert === 0;
    }

}