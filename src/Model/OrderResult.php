<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Model;

use DateTime;

/**
 * Class OrderResult
 * @package Omnipay\Omnikassa\Model
 */
class OrderResult
{
    /**
     * @var string
     */
    private $merchantOrderId;

    /**
     * @var string
     */
    private $omnikassaOrderId;

    /**
     * @var string
     */
    private $poiId;

    /**
     * @var string
     */
    private $orderStatus;

    /**
     * @var DateTime
     */
    private $orderStatusDateTime;

    /**
     * @var string
     */
    private $errorCode;

    /**
     * @var Money
     */
    private $paidAmount;

    /**
     * @var Money
     */
    private $totalAmount;

    /**
     * OrderResult constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->merchantOrderId      = $data['merchantOrderId'];
        $this->omnikassaOrderId     = $data['omnikassaOrderId'];
        $this->poiId                = $data['poiId'];
        $this->orderStatus          = $data['orderStatus'];
        $this->orderStatusDateTime  = new DateTime($data['orderStatusDateTime']);
        $this->errorCode            = $data['errorCode'];
        $this->paidAmount           = new Money($data['paidAmount']['currency'], (int)$data['paidAmount']['amount']);
        $this->totalAmount          = new Money($data['totalAmount']['currency'], (int)$data['totalAmount']['amount']);
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getMerchantOrderId(): string
    {
        return $this->merchantOrderId;
    }

    /**
     * @return string
     */
    public function getOmnikassaOrderId(): string
    {
        return $this->omnikassaOrderId;
    }

    /**
     * @return string
     */
    public function getPoiId(): string
    {
        return $this->poiId;
    }

    /**
     * @return string
     */
    public function getOrderStatus(): string
    {
        return $this->orderStatus;
    }

    /**
     * @return DateTime
     */
    public function getOrderStatusDateTime(): DateTime
    {
        return $this->orderStatusDateTime;
    }

    /**
     * @return Money
     */
    public function getPaidAmount(): Money
    {
        return $this->paidAmount;
    }

    /**
     * @return Money
     */
    public function getTotalAmount(): Money
    {
        return $this->totalAmount;
    }


}