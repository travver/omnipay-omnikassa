<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Model;

/**
 * Class OrderStatus
 * @package Omnipay\Omnikassa\Model
 */
class OrderStatus
{
    /**
     * The transaction is completed
     */
    const COMPLETED = "COMPLETED";

    /**
     * The transaction expired
     */
    const EXPIRED = "EXPIRED";

    /**
     * The transaction is still in progress
     */
    const IN_PROGRESS = "IN_PROGRESS";

    /**
     * The transaction is cancelled by the customer
     */
    const CANCELLED = "CANCELLED";

    /**
     * @param string $status
     * @return bool
     */
    public static function validate(string $status): bool
    {
        return in_array($status, [self::COMPLETED, self::EXPIRED, self::IN_PROGRESS, self::CANCELLED,], true);
    }
}