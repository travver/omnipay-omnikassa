<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa;

use Omnipay\Common\AbstractGateway;
use Omnipay\Common\GatewayInterface;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Omnikassa\Message\AcceptNotificationRequest;
use Omnipay\Omnikassa\Message\AcceptNotificationResponse;
use Omnipay\Omnikassa\Message\PurchaseRequest;

/**
 * Class Gateway
 * @package Omnipay\Omnikassa
 */
class Gateway extends AbstractGateway implements GatewayInterface
{
    use RequestParameters;

    /**
     * Get gateway display name
     * This can be used by carts to get the display name for each gateway.
     * @return string
     */
    public function getName(): string
    {
        return 'Rabobank Omnikassa 2.0';
    }

    /**
     * @return array
     */
    public function getDefaultParameters(): array
    {
        return [
            'language'              => 'NL',
            'paymentMethodForced'   => false,
            'refreshToken'          => '',
            'signingKey'            => '',
            'testMode'              => false,
            'timestamp'             => '',
        ];
    }

    /**
     * @param array $parameters
     * @return PurchaseRequest
     */
    public function purchase(array $parameters = []): PurchaseRequest
    {
        return $this->createRequest(PurchaseRequest::class, $parameters);
    }

    /**
     * @param array $parameters
     * @return AcceptNotificationRequest
     */
    public function acceptNotification(array $parameters = []): AcceptNotificationRequest
    {
        return $this->createRequest(AcceptNotificationRequest::class, $parameters);
    }
}