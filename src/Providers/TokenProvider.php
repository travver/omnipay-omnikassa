<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Providers;

use Omnipay\Common\Exception\InvalidResponseException;
use Omnipay\Omnikassa\Model\AccessToken;
use Omnipay\Common\Http\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class TokenProvider
 * @package Tests\Providers
 */
class TokenProvider
{
    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * TokenProvider constructor.
     * @param ClientInterface        $httpClient
     */
    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $refreshToken
     * @param bool   $testMode
     * @return AccessToken
     * @throws InvalidResponseException
     */
    public function getAccessToken(string $refreshToken, bool $testMode): AccessToken
    {
        $response = $this->httpClient->request('GET', $this->getEndpoint($testMode), [
            'Authorization' => 'Bearer ' . $refreshToken
        ]);

        return $this->createTokenFromResponse($response);
    }

    /**
     * @param ResponseInterface $response
     * @return AccessToken
     * @throws InvalidResponseException
     */
    private function createTokenFromResponse(ResponseInterface $response): AccessToken
    {
        $contents = $response->getBody()->getContents();

        $responseToken = json_decode($contents, true);

        if (!is_array($responseToken) ||
            !array_key_exists('token', $responseToken) ||
            !array_key_exists('validUntil', $responseToken) ||
            !array_key_exists('durationInMillis', $responseToken)
        ) {
            throw new InvalidResponseException($contents);
        }

        return new AccessToken(
            $responseToken['token'],
            $responseToken['validUntil'],
            $responseToken['durationInMillis']
        );
    }

    /**
     * @param bool $testMode
     * @return string
     */
    private function getEndpoint(bool $testMode): string
    {
        return $testMode ? $this->getTestEndpoint() : $this->getLiveEndpoint();
    }

    /**
     * @return string
     */
    private function getTestEndpoint(): string
    {
        return 'https://betalen.rabobank.nl/omnikassa-api-sandbox/gatekeeper/refresh';
    }

    /**
     * @return string
     */
    private function getLiveEndpoint(): string
    {
        return 'https://betalen.rabobank.nl/omnikassa-api/gatekeeper/refresh';
    }


}