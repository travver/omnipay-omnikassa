<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Omnikassa\Model\OrderResult;

/**
 * Class CompletePurchaseResponse
 * @package Omnipay\Omnikassa\Message
 */
class AcceptNotificationResponse extends AbstractResponse
{
    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function moreOrderResultsAvailable(): bool
    {
        return $this->data['moreOrderResultsAvailable'] === 'true';
    }

    /**
     * @return array
     */
    public function orderResults(): array
    {
        return array_map(function(array $orderResult) {
            return new OrderResult($orderResult);
        }, $this->data['orderResults']);
    }

}