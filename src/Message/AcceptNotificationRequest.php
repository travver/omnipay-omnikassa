<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Message;

use Omnipay\Common\Exception\InvalidRequestException;
use Omnipay\Common\Exception\InvalidResponseException;

/**
 * Class AcceptNotificationRequest
 * @package Omnipay\Omnikassa\Message
 */
class AcceptNotificationRequest extends AbstractRequest
{
    /**
     * @param mixed $data
     * @return AcceptNotificationResponse
     * @throws InvalidResponseException
     */
    public function sendData($data): AcceptNotificationResponse
    {
        $response = $this->httpClient->request('GET', $this->getEndpoint(), [
            'Authorization' => 'Bearer ' . $data['notificationToken']
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if ($this->signData($data) !== $data['signature']) {
            throw new InvalidResponseException("Signature invalid: '{$data['signature']}'");
        }

        return new AcceptNotificationResponse($this, $data);
    }

    /**
     * @param array $data
     * @return string
     */
    private function signData(array $data): string
    {
        $signData = [
            $data['moreOrderResultsAvailable'] ? 'true' : 'false',
        ];

        foreach($data['orderResults'] as $orderResult) {
            $signData[] = $orderResult['merchantOrderId'];
            $signData[] = $orderResult['omnikassaOrderId'];
            $signData[] = $orderResult['poiId'];
            $signData[] = $orderResult['orderStatus'];
            $signData[] = $orderResult['orderStatusDateTime'];
            $signData[] = $orderResult['errorCode'];
            $signData[] = $orderResult['paidAmount']['currency'];
            $signData[] = $orderResult['paidAmount']['amount'];
            $signData[] = $orderResult['totalAmount']['currency'];
            $signData[] = $orderResult['totalAmount']['amount'];
        }

        return $this->sign($signData);
    }

    /**
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $content = json_decode($this->httpRequest->getContent(), true);

        if (is_null($content)) {
            throw new InvalidRequestException("Invalid JSON");
        }

        if ($content['signature'] !== $this->signRequest($content)) {
            throw new InvalidRequestException("Signature invalid: '{$content['signature']}'");
        }

        return [
            'notificationToken' => $content['authentication'],
        ];
    }

    /**
     * @param array $data
     * @return string
     */
    private function signRequest(array $data): string
    {
        return $this->sign([
            $data['authentication'],
            $data['expiry'],
            $data['eventName'],
            $data['poiId'],
        ]);
    }


    /**
     * @return string
     */
    protected function getTestEndpoint(): string
    {
        return 'https://betalen.rabobank.nl/omnikassa-api-sandbox/order/server/api/events/results/merchant.order.status.changed';
    }

    /**
     * @return string
     */
    protected function getLiveEndpoint(): string
    {
        return 'https://betalen.rabobank.nl/omnikassa-api/order/server/api/events/results/merchant.order.status.changed';
    }

}