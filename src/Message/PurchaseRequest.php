<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Message;

use InvalidArgumentException;
use Omnipay\Common\Exception\InvalidResponseException;
use Omnipay\Omnikassa\Providers\TokenProvider;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use function array_key_exists;
use function implode;

/**
 * Class PurchaseRequest
 * @package Omnipay\Omnikassa\Message
 */
class PurchaseRequest extends AbstractRequest
{
    /**
     * Supported payment methods
     */
    const PAYMENT_METHODS = [
        'IDEAL',
        'AFTERPAY',
        'PAYPAL',
        'MASTERCARD',
        'VISA',
        'BANCONTACT',
        'MAESTRO',
        'V_PAY',
        'CARDS',
    ];

    /**
     * @var TokenProvider|null
     */
    private $tokenProvider;

    /**
     * @param TokenProvider $tokenProvider
     */
    public function setTokenProvider(TokenProvider $tokenProvider)
    {
        $this->tokenProvider = $tokenProvider;
    }

    /**
     * @param mixed $data
     * @return PurchaseResponse
     * @throws InvalidResponseException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function sendData($data): PurchaseResponse
    {
        if (!$this->tokenProvider instanceof TokenProvider) {
            throw new RuntimeException("No token provider initialized");
        }

        if ($this->getPaymentMethod() && !in_array($this->getPaymentMethod(), self::PAYMENT_METHODS)) {
            throw new InvalidArgumentException(sprintf(
                    "Unsupported payment method '%s' provided. Supported methods are: %s.",
                    $this->getPaymentMethod(), implode(', ', self::PAYMENT_METHODS))
            );
        }

        $accessToken = $this->tokenProvider->getAccessToken($this->getRefreshToken(), $this->getTestMode());

        $response = $this->httpClient->request('POST', $this->getEndPoint(), [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken->getToken(),
        ], json_encode($data));

        if ($response->getStatusCode() !== 201) {
            throw $this->createExceptionFromResponse(
                $response,
                [
                    'request' => [
                        'endpoint' => $this->getEndpoint(),
                        'token' => $this->getToken(),
                        'data' => $data,
                    ],
                ]
            );
        }

        $data = json_decode($response->getBody()->getContents(), true);

        if ($this->sign([$data['redirectUrl']]) !== $data['signature']) {
            throw new InvalidResponseException("Signature invalid: '{$data['signature']}'");
        }

        return new PurchaseResponse($this, $data);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [
            'amount' => [
                'amount' => $this->getAmountInteger(),
                'currency' => $this->getCurrency(),
            ],
            'description' => $this->getDescription(),
            'language' => $this->getLanguage(),
            'merchantOrderId' => $this->getTransactionId(),
            'merchantReturnURL' => $this->getReturnUrl(),
            'timestamp' => $this->getTimestamp(),
        ];

        if ($this->getParameter('paymentMethod')) {
            $data['paymentBrand'] = $this->getParameter('paymentMethod');
            $data['paymentBrandForce'] = $this->getParameter('paymentMethodForced') ? 'FORCE_ALWAYS' : 'FORCE_ONCE';
        }

        $data['signature'] = $this->signData($data);

        return $data;
    }

    /**
     * @return string
     */
    protected function getTestEndpoint(): string
    {
        return 'https://betalen.rabobank.nl/omnikassa-api-sandbox/order/server/api/order';
    }

    /**
     * @return string
     */
    protected function getLiveEndpoint(): string
    {
        return 'https://betalen.rabobank.nl/omnikassa-api/order/server/api/order';
    }

    private function createExceptionFromResponse(
        ResponseInterface $response,
        array $metaData
    ): InvalidResponseException
    {
        return new InvalidResponseException(json_encode([
            'httpStatusCode' => $response->getStatusCode(),
            'httpReasonPhrase' => $response->getReasonPhrase(),
            'responseBody' => $response->getBody()->getContents(),
            'metaData' => $metaData,
        ]), $response->getStatusCode());
    }

    /**
     * @param array $data
     * @return string
     */
    private function signData(array $data): string
    {
        $signData = [
            $data['timestamp'],
            $data['merchantOrderId'],
            $data['amount']['currency'],
            $data['amount']['amount'],
            $data['language'],
            $data['description'],
            $data['merchantReturnURL'],
        ];

        if (array_key_exists('paymentBrand', $data)) {
            $signData[] = $data['paymentBrand'];
        }

        if (array_key_exists('paymentBrandForce', $data)) {
            $signData[] = $data['paymentBrandForce'];
        }

        return $this->sign($signData);
    }
}