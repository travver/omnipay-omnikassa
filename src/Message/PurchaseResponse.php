<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Message;

use Omnipay\Common\Message\AbstractResponse;

/**
 * Class PurchaseResponse
 * @package Omnipay\Omnikassa\Message
 */
class PurchaseResponse extends AbstractResponse
{
    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isRedirect(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->data['redirectUrl'];
    }

}