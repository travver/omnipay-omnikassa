<?php

declare(strict_types=1);

namespace Omnipay\Omnikassa\Message;

use Omnipay\Common\Message\AbstractRequest as BaseAbstractRequest;
use Omnipay\Omnikassa\RequestParameters;

/**
 * Class AbstractRequest
 * @package Omnipay\Omnikassa\Message
 */
abstract class AbstractRequest extends BaseAbstractRequest
{
    use RequestParameters;

    /**
     * @return string
     */
    public function getEndpoint(): string
    {
        return $this->getTestMode() ? $this->getTestEndpoint() : $this->getLiveEndpoint();
    }

    /**
     * @param array $data
     * @return string
     */
    protected function sign(array $data): string
    {
        return hash_hmac('sha512', implode(',', array_map('strval', $data)), $this->getSigningKey());
    }

    /**
     * @return string
     */
    abstract protected function getTestEndpoint(): string;

    /**
     * @return string
     */
    abstract protected function getLiveEndpoint(): string;
}