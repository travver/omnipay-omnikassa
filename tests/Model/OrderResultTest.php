<?php

declare(strict_types=1);

namespace Tests\Model;

use DateTime;
use DateTimeZone;
use Omnipay\Omnikassa\Model\OrderResult;
use Omnipay\Omnikassa\Model\OrderStatus;
use Omnipay\Tests\TestCase;

/**
 * Class OrderResultTest
 * @package Tests\Model
 */
class OrderResultTest extends TestCase
{
    /**
     * @var array
     */
    private $validResult = [
        "merchantOrderId"     => "700000",
        "omnikassaOrderId"    => "87093384-e691-11e8-9f32-f2801f1b9fd1",
        "poiId"               => "2004",
        "orderStatus"         => "COMPLETED",
        "orderStatusDateTime" => "2018-11-12T12:00:00.000+01:00",
        "errorCode"           => "",
        "paidAmount"          => [
            "currency" => "EUR",
            "amount"   => "12595",
        ],
        "totalAmount"         => [
            "currency" => "USD",
            "amount"   => "19999",
        ],
    ];

    /**
     * Test creating a valid order result
     */
    public function testCreateOrderResult()
    {
        $orderResult = new OrderResult($this->validResult);

        $this->assertEquals("700000", $orderResult->getMerchantOrderId());
        $this->assertEquals("87093384-e691-11e8-9f32-f2801f1b9fd1", $orderResult->getOmnikassaOrderId());
        $this->assertEquals("2004", $orderResult->getPoiId());
        $this->assertEquals(OrderStatus::COMPLETED, $orderResult->getOrderStatus());
        $this->assertEquals("", $orderResult->getErrorCode());
        $this->assertEquals("EUR", $orderResult->getPaidAmount()->getCurrency());
        $this->assertEquals(12595, $orderResult->getPaidAmount()->getAmount());
        $this->assertEquals("USD", $orderResult->getTotalAmount()->getCurrency());
        $this->assertEquals(19999, $orderResult->getTotalAmount()->getAmount());

        $date = new DateTime();
        $date->setDate(2018, 11, 12);
        $date->setTimezone(new DateTimeZone("+01:00"));
        $date->setTime(12, 0, 0, 0);

        $this->assertEquals($date, $orderResult->getOrderStatusDateTime());
    }
}