<?php

declare(strict_types=1);

namespace Tests\Model;

use Omnipay\Omnikassa\Model\AccessToken;
use Omnipay\Tests\TestCase;

/**
 * Class AccessTokenTest
 * @package Tests\Model
 */
class AccessTokenTest extends TestCase
{
    /**
     * Test that isValid() returns true if the accessToken is not expired
     */
    public function testAccessTokenIsValid()
    {
        $accessToken = new AccessToken('access-token-value', date('c', time() + 600), 600000);

        $this->assertTrue($accessToken->isValid());
    }

    /**
     * Test that isValid() returns false if the accessToken is expired
     */
    public function testAccessTokenIsExpired()
    {
        $accessToken = new AccessToken('access-token-value', date('c', time() - 600), 600000);

        $this->assertFalse($accessToken->isValid());
    }


}