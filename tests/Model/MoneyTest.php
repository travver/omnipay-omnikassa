<?php

declare(strict_types=1);

namespace Tests\Money;

use Omnipay\Omnikassa\Model\Money;
use Omnipay\Tests\TestCase;

/**
 * Class MoneyTest
 * @package Tests\Money
 */
class MoneyTest extends TestCase
{
    /**
     * Test creating a new money object with an integer value
     */
    public function testMoneyFromInteger()
    {
        $money = new Money("EUR", 19999);

        $this->assertEquals("EUR", $money->getCurrency());
        $this->assertEquals(19999, $money->getAmount());
    }

    /**
     * Test creating a new money object with a decimal value
     */
    public function testMoneyFromDecimal()
    {
        $money = Money::fromDecimal("USD", 125.95);

        $this->assertEquals("USD", $money->getCurrency());
        $this->assertEquals(12595, $money->getAmount());
    }

}