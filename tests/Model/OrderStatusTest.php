<?php

declare(strict_types=1);

namespace Tests\Model;

use Omnipay\Omnikassa\Model\OrderStatus;
use Omnipay\Tests\TestCase;

/**
 * Class OrderStatusTest
 * @package Tests\Model
 */
class OrderStatusTest extends TestCase
{
    /**
     *
     */
    public function testOrderStatusValid()
    {
        $this->assertTrue(OrderStatus::validate("COMPLETED"));
        $this->assertTrue(OrderStatus::validate("EXPIRED"));
        $this->assertTrue(OrderStatus::validate("IN_PROGRESS"));
        $this->assertTrue(OrderStatus::validate("CANCELLED"));
    }

    /**
     *
     */
    public function testOrderStatusInvalid()
    {
        $this->assertFalse(OrderStatus::validate(""));
        $this->assertFalse(OrderStatus::validate("INVALID_STATUS"));
    }

}