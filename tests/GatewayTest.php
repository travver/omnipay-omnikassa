<?php

declare(strict_types=1);

namespace Tests;

use Omnipay\Omnikassa\Gateway;
use Omnipay\Omnikassa\Message\AcceptNotificationRequest;
use Omnipay\Omnikassa\Message\PurchaseRequest;
use Omnipay\Tests\GatewayTestCase;

/**
 * Class GatewayTest
 * @package Tests
 */
class GatewayTest extends GatewayTestCase
{
    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->gateway = new Gateway($this->getHttpClient(), $this->getHttpRequest());
    }

    /**
     *
     */
    public function testPurchase()
    {
        $request = $this->gateway->purchase(['amount' => '12.95']);

        $this->assertInstanceOf(PurchaseRequest::class, $request);

        $this->assertSame('12.95', $request->getAmount());
    }

    /**
     *
     */
    public function testAcceptNotification()
    {
        $request = $this->gateway->acceptNotification();

        $this->assertInstanceOf(AcceptNotificationRequest::class, $request);
    }

    public function testDefaultParametersHaveMatchingMethods()
    {
        $settings = $this->gateway->getDefaultParameters();

        foreach ($settings as $key => $default) {
            $getter = 'get' . ucfirst($this->camelCase($key));
            $setter = 'set' . ucfirst($this->camelCase($key));

            $value = $this->getTestValue($default);

            $this->assertTrue(method_exists($this->gateway, $getter), "Gateway must implement $getter()");
            $this->assertTrue(method_exists($this->gateway, $setter), "Gateway must implement $setter()");

            // setter must return instance
            $this->assertSame($this->gateway, $this->gateway->$setter($value));
            $this->assertSame($value, $this->gateway->$getter());
        }
    }

    public function testPurchaseParameters()
    {
        if ($this->gateway->supportsPurchase()) {
            foreach ($this->gateway->getDefaultParameters() as $key => $default) {
                // set property on gateway
                $getter = 'get' . ucfirst($this->camelCase($key));
                $setter = 'set' . ucfirst($this->camelCase($key));
                $value = $this->getTestValue($default);
                $this->gateway->$setter($value);

                // request should have matching property, with correct value
                $request = $this->gateway->purchase();
                $this->assertSame($value, $request->$getter());
            }
        }
    }

    /**
     * @param $default
     * @return bool|int|string
     * @throws \Exception
     */
    public function getTestValue($default)
    {
        switch (gettype($default)) {
            case 'boolean':
                $value = (bool)random_int(0, 1);
                break;
            case 'integer':
                $value = random_int(0, PHP_INT_MAX);
                break;
            case 'array':
                $value = count($default) > 0 ? $default[array_rand($default)] : [];
                break;
            case 'string':
            default:
                $value = uniqid();
        }

        return $value;
    }
}