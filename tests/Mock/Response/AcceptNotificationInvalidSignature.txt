HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "moreOrderResultsAvailable": false,
    "orderResults": [{
        "merchantOrderId": "700000",
        "omnikassaOrderId": "87093384-e691-11e8-9f32-f2801f1b9fd1",
        "poiId": "2004",
        "orderStatus": "COMPLETED",
        "orderStatusDateTime": "2018-11-12T12:00:00.000+01:00",
        "errorCode": "",
        "paidAmount": {
            "currency": "EUR",
            "amount": "19999"
        },
        "totalAmount": {
            "currency": "EUR",
            "amount": "19999"
        }
    }],
    "signature": "invalid-response-signature-value"
}