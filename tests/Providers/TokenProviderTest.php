<?php

declare(strict_types=1);

namespace Tests\Providers;

use Mockery;
use Omnipay\Common\Exception\InvalidResponseException;
use Omnipay\Common\Http\ClientInterface;
use Omnipay\Omnikassa\Model\AccessToken;
use Omnipay\Omnikassa\Providers\TokenProvider;
use Omnipay\Tests\TestCase;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class TokenProviderTest
 * @package Tests\Providers
 */
class TokenProviderTest extends TestCase
{
    /**
     * @var TokenProvider
     */
    private $tokenProvider;

    /**
     * @var Mockery\Mock
     */
    private $cacheMock;

    /**
     * @var Mockery\Mock
     */
    private $httpClientMock;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->httpClientMock = Mockery::spy(ClientInterface::class);

        $this->tokenProvider = new TokenProvider($this->httpClientMock);
    }

    public function testTokenProviderThrowsExceptionIfResponseIsInvalid()
    {
        $cacheItem = Mockery::mock(CacheItemInterface::class);
        $cacheItem->shouldReceive('isHit')->andReturn(false);

        $cacheDriver = Mockery::mock(CacheItemPoolInterface::class);
        $cacheDriver->shouldReceive('getItem')->andReturn($cacheItem);

        $response = Mockery::mock(ResponseInterface::class);
        $response->shouldReceive('getBody')->andReturn(new class {
            public function getContents() {
                return '{}';
            }
        });

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->shouldReceive('request')->andReturn($response);

        $tokenProvider = new TokenProvider($httpClient);

        $this->expectException(InvalidResponseException::class);

        $tokenProvider->getAccessToken('1234567890', false);
    }

}