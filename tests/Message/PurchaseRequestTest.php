<?php

declare(strict_types=1);

namespace Tests\Message;

use InvalidArgumentException;
use Mockery;
use Omnipay\Common\Exception\InvalidResponseException;
use Omnipay\Omnikassa\Message\PurchaseRequest;
use Omnipay\Omnikassa\Model\AccessToken;
use Omnipay\Omnikassa\Providers\TokenProvider;

use RuntimeException;

/**
 * Class PurchaseRequestTest
 * @package Tests\Message
 */
class PurchaseRequestTest extends RequestTestCase
{
    /**
     * @var PurchaseRequest
     */
    private $request;

    /**
     * @var string
     */
    private $timestamp;

    /**
     * @var
     */
    private $tokenProviderMock;

    /**
     * Set up a default purchase request
     */
    protected function setUp(): void
    {
        $this->timestamp = date('c');

        $this->request = new PurchaseRequest($this->getHttpClient(), $this->getHttpRequest());

        $this->request->initialize([
            'testMode'      => true,
            'amount'        => '12.95',
            'currency'      => 'EUR',
            'description'   => 'Test description',
            'language'      => 'NL',
            'refreshToken'  => 'eyJraWQiOiJHS0wiLCJhbGciOiJFUzI1NiJ9',
            'returnUrl'     => 'https://www.example.com/return',
            'signingKey'    => 'WMohxvYp3fzYLFgit2bX',
            'timestamp'     => $this->timestamp,
            'transactionId' => '700000',
        ]);

        $this->tokenProviderMock = Mockery::mock(TokenProvider::class);
        $this->tokenProviderMock->shouldReceive('getAccessToken')->zeroOrMoreTimes()->andReturn(
            new AccessToken('valid-access-token', date('c', time() + 600), 600000)
        );
    }

    /**
     * Assert that the getData method returns the correct request for Omnikassa
     */
    public function testGetData()
    {
        $data = $this->request->getData();

        $this->assertEquals($this->timestamp, $data['timestamp']);
        $this->assertEquals('700000', $data['merchantOrderId']);
        $this->assertEquals('1295', $data['amount']['amount']);
        $this->assertEquals('EUR', $data['amount']['currency']);
        $this->assertEquals('NL', $data['language']);
        $this->assertEquals('https://www.example.com/return', $data['merchantReturnURL']);
        $this->assertEquals('Test description', $data['description']);

        $signData = implode(',', [
            $this->timestamp,                   // Timestamp
            '700000',                           // Merchant order id
            'EUR',                              // Amount currency
            '1295',                             // Amount amount
            'NL',                               // Language
            'Test description',                 // Description
            'https://www.example.com/return',   // Merchant Return URL
        ]);

        $signature = hash_hmac('sha512', $signData, 'WMohxvYp3fzYLFgit2bX');

        $this->assertEquals($signature, $data['signature']);
    }

    /**
     * Test sending a purchase request, asserting a correct response
     */
    public function testSend()
    {
        $this->setMockHttpResponse("Response/PurchaseSuccess.txt");

        $this->request->setTokenProvider($this->tokenProviderMock);

        $response = $this->request->send();

        $this->assertInstanceOf(\Omnipay\Omnikassa\Message\PurchaseResponse::class, $response);
        $this->assertFalse($response->isSuccessful());
        $this->assertTrue($response->isRedirect());

        $redirectUrl = 'https://betalen.rabobank.nl/omnikassa-api/payment-brand?token=iqaEpyFqixRGM47N2ore';

        $this->assertEquals(
            $redirectUrl,
            $response->getRedirectUrl()
        );
    }

    /**
     * Test that an exception is thrown when the token provider is not initialized before sending the request
     */
    public function testMissingTokenProviderException()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("No token provider initialized");

        $this->request->send();
    }

    /**
     * Test that an exception is thrown when an invalid signature is provided by the payment provider
     */
    public function testInvalidResponseSignature()
    {
        $this->setMockHttpResponse("Response/PurchaseInvalidResponseSignature.txt");

        $this->request->setTokenProvider($this->tokenProviderMock);

        $this->expectException(InvalidResponseException::class);
        $this->expectExceptionMessage("Signature invalid: 'invalid-response-signature-value'");

        $this->request->send();
    }

    /**
     * Test that an exception is thrown when an invalid signature is used to sign the request
     */
    public function testInvalidRequestSignature()
    {
        $this->setMockHttpResponse("Response/PurchaseInvalidRequestSignature.txt");

        $this->request->setTokenProvider($this->tokenProviderMock);

        $this->expectException(InvalidResponseException::class);
        $this->expectExceptionCode(5001);
        $this->expectExceptionMessage("Invalid or missing signature");

        $this->request->send();
    }

    /**
     *
     */
    public function testWithoutPaymentMethod()
    {
        $data = $this->request->getData();

        $this->assertArrayNotHasKey('paymentBrand', $data);
        $this->assertArrayNotHasKey('paymentBrandForce', $data);
    }

    /**
     *
     */
    public function testPaymentMethodForced()
    {
        $this->request->setPaymentMethod('IDEAL');
        $this->request->setPaymentMethodForced(true);

        $data = $this->request->getData();

        $this->assertArrayHasKey('paymentBrand', $data);
        $this->assertArrayHasKey('paymentBrandForce', $data);

        $this->assertEquals($data['paymentBrand'], 'IDEAL');
        $this->assertEquals($data['paymentBrandForce'], 'FORCE_ALWAYS');
    }

    /**
     *
     */
    public function testPaymentMethodNotForced()
    {
        $this->request->setPaymentMethod('PAYPAL');

        $data = $this->request->getData();

        $this->assertArrayHasKey('paymentBrand', $data);
        $this->assertArrayHasKey('paymentBrandForce', $data);

        $this->assertEquals($data['paymentBrand'], 'PAYPAL');
        $this->assertEquals($data['paymentBrandForce'], 'FORCE_ONCE');
    }

    public function testInvalidPaymentMethodThrowsException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Unsupported payment method 'UNSUPPORTED_METHOD' provided. Supported methods" .
            " are: IDEAL, AFTERPAY, PAYPAL, MASTERCARD, VISA, BANCONTACT, MAESTRO, V_PAY, CARDS");

        $this->request->setTokenProvider($this->tokenProviderMock);
        $this->request->setPaymentMethod('UNSUPPORTED_METHOD');
        $this->request->send();
    }

    /**
     * Test if the correct endpoint is returned when switching between test and live mode
     */
    public function testGetEndPoint()
    {
        $this->request->setTestMode(false);
        $this->assertStringContainsString('/omnikassa-api/', $this->request->getEndPoint());

        $this->request->setTestMode(true);
        $this->assertStringContainsString('/omnikassa-api-sandbox/', $this->request->getEndPoint());
    }

}