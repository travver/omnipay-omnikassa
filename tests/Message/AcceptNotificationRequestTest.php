<?php

declare(strict_types=1);

namespace Tests\Message;

use Omnipay\Common\Exception\InvalidRequestException;
use Omnipay\Common\Exception\InvalidResponseException;
use Omnipay\Omnikassa\Message\AcceptNotificationRequest;
use Omnipay\Omnikassa\Message\AcceptNotificationResponse;

/**
 * Class AcceptNotificationTest
 * @package Tests\Message
 */
class AcceptNotificationRequestTest extends RequestTestCase
{
    /**
     * @var
     */
    private $request;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->request = new AcceptNotificationRequest($this->getHttpClient(), $this->getHttpRequest());

        $this->request->initialize([
            'signingKey'        => 'WMohxvYp3fzYLFgit2bX',
        ]);
    }

    /**
     *
     */
    public function testGetData()
    {
        $this->setMockHttpRequest("Request/AcceptNotificationSuccess.txt");

        $response = $this->request->getData();

        $this->assertEquals(["notificationToken" => "sRkkfMj3VfmqfhQ66sN2"], $response);
    }

    /**
     *
     */
    public function testSend()
    {
        $this->setMockHttpRequest("Request/AcceptNotificationSuccess.txt");
        $this->setMockHttpResponse("Response/AcceptNotificationSuccess.txt");

        $response = $this->request->send();

        $this->assertInstanceOf(AcceptNotificationResponse::class, $response);

        $this->assertEquals(false, $response->moreOrderResultsAvailable());
        $this->assertCount(1, $response->orderResults());
    }

    /**
     *
     */
    public function testInvalidRequest()
    {
        $this->setMockHttpRequest("Request/AcceptNotificationInvalidRequest.txt");

        $this->expectException(InvalidRequestException::class);
        $this->expectExceptionMessage("Invalid JSON");

        $this->request->send();
    }

    /**
     * Test that an exception is thrown when an invalid signature is provided by the payment provider
     */
    public function testInvalidRequestSignature()
    {
        $this->setMockHttpRequest("Request/AcceptNotificationInvalidSignature.txt");

        $this->expectException(InvalidRequestException::class);

        $this->expectExceptionMessage("Signature invalid: 'invalid-request-signature-value'");

        $this->request->send();
    }

    /**
     * Test that an exception is thrown when an invalid signature is provided by the payment provider
     */
    public function testInvalidResponseSignature()
    {
        $this->setMockHttpRequest("Request/AcceptNotificationSuccess.txt");
        $this->setMockHttpResponse("Response/AcceptNotificationInvalidSignature.txt");

        $this->expectException(InvalidResponseException::class);
        $this->expectExceptionMessage("Signature invalid: 'invalid-response-signature-value'");

        $this->request->send();
    }

    /**
     * Test if the correct endpoint is returned when switching between test and live mode
     */
    public function testGetEndPoint()
    {
        $this->request->setTestMode(false);
        $this->assertStringContainsString('/omnikassa-api/', $this->request->getEndPoint());

        $this->request->setTestMode(true);
        $this->assertStringContainsString('/omnikassa-api-sandbox/', $this->request->getEndPoint());
    }

}