<?php

declare(strict_types=1);

namespace Tests\Message;

use Omnipay\Tests\TestCase;
use ReflectionObject;

/**
 * Class RequestTestCase
 * @package Tests\Message
 */
abstract class RequestTestCase extends TestCase
{
    /**
     * @param string $path
     */
    public function setMockHttpRequest(string $path)
    {
        $request = $this->getMockHttpRequest($path);

        $this->getHttpRequest()->initialize(
            [],
            [],
            [],
            [],
            [],
            [],
            $request->getBody()->getContents()
        );
    }

    /**
     * @param string $path
     * @return \GuzzleHttp\Psr7\Request
     */
    private function getMockHttpRequest(string $path)
    {
        $ref = new ReflectionObject($this);
        $dir = dirname($ref->getFileName());

        if (!file_exists($dir.'/Mock/'.$path) && file_exists($dir.'/../Mock/'.$path)) {
            return \GuzzleHttp\Psr7\parse_request(file_get_contents($dir.'/../Mock/'.$path));
        }

        return \GuzzleHttp\Psr7\parse_request(file_get_contents($dir.'/Mock/'.$path));
    }

}